<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function index(Request $request)
    {
        /**
         * Haystack
         */
        $contextKeyword = array('Hello', 'Hi', 'Goodbye', 'bye');


        /**
         * @param string $conversation_id
         * @param string $message
         */
        $requestMessage = $request->only('message');
        $requestConversatioId = $request->only('conversation_id');

        /**
         * @var array $message[]
         * @var string $noContext
         */
        $message = [];
        $noContext = "";

        foreach($contextKeyword as $token) {

            /**
             * Search Haystack
             * @var $pos
             */
            $pos = stripos($requestMessage['message'], $token);

            if($pos !== FALSE) {

                /**
                 * If there is a hit,
                 * Assign corresponding response based on the Context Table
                 * @var array $message[]
                 * @var string $message => 'Context Message'
                 * @var string $position => 'Occurence Position'
                 * @var string $token => 'Needle'
                 */

                if($token == 'Hello' || $token == 'Hi') {
                    $message[] = array('message' => 'Welcome to StationFive.', 'position' => $pos, 'token' => $token);
                } else if($token === 'Goodbye' || $token === 'bye' ) {
                    $message[] = array('message' => 'Thank you, see you around.', 'position' => $pos, 'token' => $token);
                }



                /**
                 * @var string $position
                 * Position on the search occurence of every matched words
                 */
                $position = array_column($message, 'position');

                /**
                 * Sort the positions accordingly
                 * Based on their Occurence
                 * For displaying - "first context takes priority" rule
                 */
                array_multisort($position, SORT_ASC, $message);



            } else {
                /**
                 * @var string $noContext
                 * No Hit
                 * Assign No Context
                 */
                 $noContext = "Sorry, I don’t understand.";
            }


        }


        if(!empty($message) ) {
            /**
             * @var array message[]
             * If not empty,
             * Matched Context Response
             */
            foreach($message as $mess => $key) {
                $response[] = $key['message'];
            }
        } else {

            /**
             * No Context Response
             */
            return response()->json([
                'response_id' => $requestConversatioId['conversation_id'],
                'response' => $noContext
            ]);
        }


        /**
         * @return \Illuminate\Http\Response
         */
        return response()->json([
            'response_id' => $requestConversatioId['conversation_id'],
            'response' => $response
        ]);

        /**
         * Issue:
         * Goodbye - will return two hits: One for Goodbye and one for bye
         * e.g: I am Ruther, Goodbye.
         * Expected Response: Thank you, see you around.
         * Current Response: Thank you, see you around. Thank you, see you around.
         */
    }
}
